<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Models\PostComment;
use App\Models\PostLike;


class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('is_active', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    public function create()
    {
        if(Auth::user()) {
            return view('posts.create');
        } else {
            return redirect('/login');
        }
    }

    public function store(Request $request)
    {
        if(Auth::user()) {
            $post = new Post;

            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);
            $post->is_active = true;

            $post->save();

            return redirect(route('posts.index'));
        } else {
            return redirect('/login');
        }
    }
    
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function edit($id) {
        return view('posts.edit', [
            'post' => Post::where('id', $id)->first()
        ]);
    }

    public function update(Request $request, $id)
    {
        Post::where('id', $id)->update([
            'title' => $request->title,
            'content' => $request->content
        ]);
    
        return redirect(route('posts.show', $id));
    }

    public function destroy($id)
    {
        Post::destroy($id);

        return back()->with('delete', 'Post has been Successfully Deleted.');
    }

    public function myPosts()
    {
        if(Auth::user()) {
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function featured()
    {
        return view('welcome', [
            'posts' => Post::inRandomOrder()->take(3)->get()
        ]);
    }

    public function archive($id)
    {
        $post = Post::find($id);

        if($post->is_active) {
            $post->is_active = false;
            $post->save();

            return back()->with('archived', 'Post has been Successfully Archived.');
        } else {
            $post->is_active = true;
            $post->save();
            
            return redirect(route('posts.myposts'))->with('unarchived', 'Post has been Successfully Unarchived.');
        }
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if($post->user_id != $user_id) {
            if($post->likes->contains('user_id', $user_id)) {
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                $postLike = new PostLike;

                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect(route('posts.show', $id));
        }
    }

    public function comment(Request $request, $id)
    {

        $post = Post::find($id);
        $user_id = Auth::user()->id;

        $postComment = new PostComment;

        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->context = $request->comment;

        $postComment->save();

        return redirect(route('posts.show', $id));
    }
}
