@extends('layouts.app')

@section('content')

	<div class="card mb-4">
		<div class="card-body">
			
			<h2 class="card-title">{{ $post->title }}</h2>
			<p class="card-subtitle text-muted">Author: {{ $post->user->name }}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{ $post->content }}</p>

            @if (Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="{{ route('posts.like', $post->id) }}">
                    @csrf
                    @method('PUT')
                    
                    @if ($post->likes->contains('user_id', Auth::id()))
                        <button class="btn btn-danger" type="submit">Unlike</button>
                    @else
                        <button class="btn btn-success" type="submit">Like</button>
                    @endif
                </form>
            @endif

            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ModalCreate">Post Comment</button>

			<div class="mt-3">
				<a href="/myPosts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>

    <div>
        <h2 class="ps-3">Comments</h2>
        @foreach ($post->comments as $comment)
            <div class="card mb-2">
                <div class="card-body">
                    <h4 class="card-text mb-1">{{ $comment->context }}</h4>
                    <h6 class="card-text text-muted">Commented By: {{ $comment->user->name }} @ {{ $comment->created_at }}</h6>
                </div>
            </div>
        @endforeach
    </div>

    @include('posts.modal.create')
@endsection
