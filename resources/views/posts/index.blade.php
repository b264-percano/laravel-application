@extends('layouts.app');

@section('content')
    @if (session()->has('archived'))
        <div class="text-center pb-10">
            <div class="border px-4 py-3 text-warning">
                {{ session()->get('archived') }}
            </div>
        </div>
    @elseif (session()->has('unarchived'))
        <div class="text-center pb-10">
            <div class="border px-4 py-3 text-success">
                {{ session()->get('unarchived') }}
            </div>
        </div>
    @elseif (session()->has('delete'))
        <div class="text-center pb-10">
            <div class="border px-4 py-3 text-danger">
                {{ session()->get('delete') }}
            </div>
        </div>
    @endif

    @if (count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card text-center mb-3">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{ $post->id }}">
                            {{ $post->title }}
                        </a>
                    </h4>
                    <h6 class="card-text mb-3">Author: {{ $post->user->name }}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{ $post->created_at }}</p>
                </div>

                @if (Auth::user())
                    @if (Auth::user()->id == $post->user_id)
                        <div class="d-flex justify-content-center">
                            <form action="{{ route('posts.archive', $post->id) }}" method="POST">
                                @csrf
                                @method('PATCH')

                                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary">Edit Post</a>
                                @if ($post->is_active)
                                    <button type="submit" class="btn btn-warning me-1">Archive Post</button>
                                @else
                                    <button type="submit" class="btn btn-success me-1">Unarchive Post</button>
                                @endif
                            </form>
                            <form action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete Post</button>
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show...</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif
@endsection
